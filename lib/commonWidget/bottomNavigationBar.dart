import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:new_news_app/commonWidget/bloc/navigation_state.dart';

import 'bloc/navigation_bloc.dart';
import 'bloc/navigation_event.dart';

class BottomNavigationBarWidget extends StatelessWidget {
  final PageController controller;

  const BottomNavigationBarWidget({Key key, this.controller}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NavigationBloc, NavigationState>(
        // ignore: missing_return
        builder: (context, state) {
      if (state is Opened) {
        return BottomNavigationBar(
          backgroundColor: Theme.of(context).bottomAppBarColor,
          currentIndex: state.pageIndex,
          fixedColor: Theme.of(context).colorScheme.secondary,
          type: BottomNavigationBarType.fixed,
          unselectedItemColor: Theme.of(context).disabledColor,
          onTap: (pageIndex) {
            BlocProvider.of<NavigationBloc>(context)
                .add(Navigate(pageIndex: pageIndex));

            controller.animateToPage(pageIndex,
                duration: Duration(milliseconds: 300), curve: Curves.linear);
          },
          items: [
            BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Home'),
            BottomNavigationBarItem(
                icon: Icon(Icons.category), label: 'Category'),
            BottomNavigationBarItem(icon: Icon(Icons.source), label: 'Sources'),
            BottomNavigationBarItem(icon: Icon(Icons.search), label: 'Search'),
            BottomNavigationBarItem(icon: Icon(Icons.person), label: 'Profile'),
          ],
          elevation: 10,
        );
      }
    });
  }
}
