import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:new_news_app/commonWidget/bloc/navigation_event.dart';
import 'navigation_state.dart';

class NavigationBloc extends Bloc<NavigationEvent, NavigationState> {
  NavigationBloc(NavigationState initialState) : super(initialState);

  @override
  Stream<NavigationState> mapEventToState(NavigationEvent event) async* {
    if (event is Navigate) {
      yield Opened(pageIndex: event.pageIndex);
    }
  }
}
