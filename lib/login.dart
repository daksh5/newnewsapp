import 'package:flutter/material.dart';

import 'auth/provider.dart';
import 'auth/validation.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final formKey = GlobalKey<FormState>();

  String _email, _password;
  FormType _formType = FormType.login;

  bool validate() {
    final form = formKey.currentState;
    form.save();
    if (form.validate()) {
      form.save();
      return true;
    } else {
      return false;
    }
  }

  void submit() async {
    if (validate()) {
      try {
        final auth = Provider.of(context).auth;
        if (_formType == FormType.login) {
          String userId = await auth.signInWithEmailAndPassWord(
            _email,
            _password,
          );

          print('Signed in $userId');
        } else {
          String userId = await auth.createUserWithEmailAndPassword(
            _email,
            _password,
          );

          print('Registered in $userId');
        }
      } catch (e) {
        print(e);
      }
    }
  }

  void switchFormState(String state) {
    formKey.currentState.reset();

    if (state == 'register') {
      setState(() {
        _formType = FormType.register;
      });
    } else {
      setState(() {
        _formType = FormType.login;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Padding(
      padding: EdgeInsets.symmetric(horizontal: 8, vertical: 0),
      child: Center(
        child: Form(
          key: formKey,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              ...buildInputs(),
              ...buildButtons(),
            ],
          ),
        ),
      ),
    ));
  }

  List<Widget> buildInputs() {
    return [
      TextFormField(
        validator: EmailValidator.validate,
        decoration: InputDecoration(labelText: 'Email'),
        onSaved: (value) => _email = value,
      ),
      TextFormField(
        validator: PasswordValidator.validate,
        decoration: InputDecoration(labelText: 'Password'),
        obscureText: true,
        onSaved: (value) => _password = value,
      ),
    ];
  }

  List<Widget> buildButtons() {
    if (_formType == FormType.login) {
      return [
        Padding(
            padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
            child: ElevatedButton(
              child: Text('Login'),
              style: ElevatedButton.styleFrom(
                  primary: Colors.purple,
                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 10),
                  textStyle:
                      TextStyle(fontSize: 15, fontWeight: FontWeight.bold)),
              onPressed: submit,
            )),
        Padding(
            padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
            child: TextButton(
              child: Text('Register Account'),
              style: TextButton.styleFrom(
                  primary: Colors.indigoAccent,
                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 10),
                  textStyle:
                      TextStyle(fontSize: 15, fontWeight: FontWeight.bold)),
              onPressed: () {
                switchFormState('register');
              },
            )),
        Divider(
          height: 50.0,
        ),
        Padding(
            padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
            child: ElevatedButton(
              child: Text("Sign in with Google"),
              style: ElevatedButton.styleFrom(
                  primary: Colors.lightGreen,
                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 10),
                  textStyle:
                      TextStyle(fontSize: 15, fontWeight: FontWeight.bold)),
              onPressed: () async {
                try {
                  final _auth = Provider.of(context).auth;
                  final id = await _auth.signInWithGoogle();
                  print('signed in with google $id');
                } catch (e) {
                  print(e);
                }
              },
            )),
      ];
    } else {
      return [
        Padding(
            padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
            child: ElevatedButton(
              child: Text('Create Account'),
              style: TextButton.styleFrom(
                  primary: Colors.indigoAccent,
                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 10),
                  textStyle:
                      TextStyle(fontSize: 15, fontWeight: FontWeight.bold)),
              onPressed: submit,
            )),
        Padding(
            padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
            child: TextButton(
              child: Text('Go to Login'),
              style: TextButton.styleFrom(
                  primary: Colors.indigoAccent,
                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 10),
                  textStyle:
                      TextStyle(fontSize: 15, fontWeight: FontWeight.bold)),
              onPressed: () {
                switchFormState('login');
              },
            ))
      ];
    }
  }
}
