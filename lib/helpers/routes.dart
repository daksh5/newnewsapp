import 'package:flutter/material.dart';
import 'package:new_news_app/models/source.dart';
import 'package:new_news_app/pages/category/categoryPage.dart';
import 'package:new_news_app/pages/homePage/homePage.dart';
import 'package:new_news_app/pages/newsApp.dart';
import 'package:new_news_app/pages/newsDetail/newsDetailPage.dart';
import 'package:new_news_app/pages/profile/profilePage.dart';
import 'package:new_news_app/pages/searchBar/serachData.dart';
import 'package:new_news_app/pages/sourceNewsPage/sourceNewPage.dart';
import 'package:new_news_app/pages/sources/sourcePage.dart';

class Routes {
  static Map<String, WidgetBuilder> getRoute() {
    return <String, WidgetBuilder>{
      '/': (_) => NewsApp(),
      '/home': (_) => HomePage(),
      '/detail': (_) => NewsDetailPage(),
      '/category': (_) => CategoryPage(),
      '/sources': (_) => SourcePage(),
      '/profile': (_) => ProfilePage(),
      '/sourcenews': (_) => SourceNewsPage(),
      '/search': (_) => SearchPage(),
    };
  }
}
