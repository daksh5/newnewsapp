
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:new_news_app/pages/homePage/bloc/newsBloc.dart';
import 'package:new_news_app/pages/homePage/bloc/newsEvent.dart';
import 'package:new_news_app/pages/homePage/bloc/newsState.dart';
import 'package:new_news_app/pages/newsDetail/bloc/detail_bloc.dart';
import 'package:new_news_app/pages/newsDetail/bloc/detail_state.dart';
import 'package:new_news_app/pages/searchBar/bloc/bloc.dart';
import 'package:new_news_app/pages/searchBar/bloc/sourceBloc.dart';
import 'package:new_news_app/pages/sourceNewsPage/bloc/newsource_bloc.dart';
import 'package:new_news_app/pages/sourceNewsPage/bloc/newsource_state.dart';
import 'package:new_news_app/pages/sources/bloc/sourceBloc.dart';
import 'package:new_news_app/pages/sources/bloc/sourceEvent.dart';
import 'package:new_news_app/pages/sources/bloc/sourceState.dart';
import 'package:new_news_app/resources/repository.dart';
import 'package:new_news_app/theme/bloc/theme_bloc.dart';
import 'package:new_news_app/theme/bloc/theme_state.dart';
import 'package:new_news_app/theme/theme.dart';

import 'auth/auth.dart';
import 'auth/provider.dart';
import 'commonWidget/bloc/navigation_bloc.dart';
import 'commonWidget/bloc/navigation_state.dart';
import 'helpers/routes.dart';
import 'login.dart';

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final Auth auth = Provider.of(context).auth;
    return StreamBuilder<String>(
      stream: auth.onAuthStateChanged,
      builder: (context, AsyncSnapshot<String> snapshot) {
        if (snapshot.connectionState == ConnectionState.active) {
          final bool loggedIn = snapshot.hasData;
          if (loggedIn == true) {
            return HomePage();
          } else {
            return LoginPage();
          }
        }
        return CircularProgressIndicator();
      },
    );
  }
}

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ThemeData apptheme;
    return MultiBlocProvider(
        providers: [
          BlocProvider<NewsBloc>(
            create: (context) =>
                NewsBloc(Loading(), Repository())..add(Fetch(type: 'General')),
          ),
          BlocProvider<SourceBloc>(
            create: (context) => SourceBloc(LoadingData(), Repository())
              ..add(FetchData(type: 'General')),
          ),
          BlocProvider<SearchBloc>(
            create: (context) => SearchBloc(LoadingSearchData(), Repository())
              ..add(FetchSearchData(type: '')),
          ),
          BlocProvider<DetailBloc>(
              create: (context) => DetailBloc(LoadingDetail())),
          BlocProvider<NewSourceBloc>(
              create: (context) => NewSourceBloc(LoadingDetails())),
          BlocProvider<NavigationBloc>(
              create: (context) => NavigationBloc(Opened(pageIndex: 0))),
          BlocProvider<ThemeBloc>(
              create: (context) =>
                  ThemeBloc(SelectedTheme(themeType: ThemeType.Light))),
        ],
        child: BlocBuilder<ThemeBloc, ThemeState>(
          builder: (context, state) {
            if (state is SelectedTheme) {
              apptheme = AppTheme.darkTheme;
            }
            return Builder(
              builder: (context) {
                return MaterialApp(
                  title: '',
                  theme: apptheme,
                  debugShowCheckedModeBanner: false,
                  routes: Routes.getRoute(),
                );
              },
            );
          },
        ));
  }
}
