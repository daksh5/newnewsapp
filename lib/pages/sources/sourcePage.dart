import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:new_news_app/models/articles.dart';
import 'package:new_news_app/pages/sourceNewsPage/bloc/newsource_bloc.dart';
import 'package:new_news_app/pages/sourceNewsPage/bloc/newsource_event.dart';
import 'package:new_news_app/pages/sources/bloc/bloc.dart';
import 'package:new_news_app/pages/sources/bloc/sourceState.dart';
import 'package:new_news_app/theme/theme.dart';

class SourcePage extends StatefulWidget {
  final PageController controller;

  SourcePage({Key key, this.controller}) : super(key: key);

  @override
  _SourcePageState createState() => _SourcePageState();
}

class _SourcePageState extends State<SourcePage> {
  Widget _categoryCard(String text, String type, String imgPath,List<Articles>article) {
    return InkWell(
        onTap: () {
          final detailBloc = BlocProvider.of<NewSourceBloc>(context);
          detailBloc.add(SelectNewsForDetail(article: article,type: text));
          Navigator.pushNamed(context, '/sourcenews');
        },
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(10)),
              child: Container(
                  // height:50,
                  color: Theme.of(context).primaryColor,
                  child: Padding(
                      padding: EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                      child: Text(text,
                          style:
                              AppTheme.h6Style.copyWith(color: Colors.white)))),
            )
          ],
        ));
  }

  Widget _body(
    BuildContext context,
    List<Articles> list, {
    String type,
  }) {
    var map1 = Map.fromIterable(list, key: (e) => e.source.name);
    print(map1);
    return CustomScrollView(
      slivers: <Widget>[
        SliverAppBar(
          centerTitle: true,
          title: Text(
            'News Sources',
            style: AppTheme.h2Style
                .copyWith(color: Theme.of(context).textSelectionColor),
          ),
          backgroundColor: Theme.of(context).bottomAppBarColor,
          pinned: true,
        ),
        SliverGrid(
            gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
              maxCrossAxisExtent: 200.0,
              mainAxisSpacing: 10.0,
              crossAxisSpacing: 10.0,
              childAspectRatio: 4.0,
            ),
            delegate: SliverChildBuilderDelegate(
                (context, index) =>
                    _categoryCard(list[index].source.name, "", "",list),
                childCount: list.length))
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        systemNavigationBarColor: Theme.of(context).primaryColorDark,
        statusBarColor: Theme.of(context).primaryColorDark));
    return Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        body: SafeArea(child:
            BlocBuilder<SourceBloc, SourceState>(builder: (context, state) {
          if (state == null) {
            return Center(child: Text('Null block'));
          }
          if (state is FailedToLoad) {
            return Center(child: Text('Something went wrong'));
          }
          if (state is DataLoaded) {
            if (state.items == null || state.items.isEmpty) {
              return Text('No content available');
            } else {
              return _body(context, state.items, type: state.type);
            }
          } else {
            return Center(child: CircularProgressIndicator());
          }
        })));
  }
}
