import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:new_news_app/models/articles.dart';

abstract class SourceState extends Equatable {
  const SourceState();

  @override
  List<Object> get props => [];
}

class LoadingData extends SourceState {}

class DataLoaded extends SourceState {
  final List<Articles> items;
  final String type;

  const DataLoaded({@required this.items, this.type});

  @override
  List<Object> get props => [items];

  @override
  String toString() => 'Loaded { items: ${items.length} }';
}

class FailedToLoad extends SourceState {}
