import 'dart:async';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:new_news_app/resources/repository.dart';
import 'sourceEvent.dart';
import 'sourceState.dart';

class SourceBloc extends Bloc<SourceEvent, SourceState> {
  final Repository repository;

  SourceBloc(SourceState initialState, this.repository) : super(initialState);

  @override
  Stream<SourceState> mapEventToState(SourceEvent event) async* {
    if (event is FetchData) {
      try {
        yield LoadingData();
        final items = await repository.fetchAllNews(category: event.type);
        yield DataLoaded(items: items, type: event.type);
      } catch (_) {
        yield FailedToLoad();
      }
    }
  }
}
