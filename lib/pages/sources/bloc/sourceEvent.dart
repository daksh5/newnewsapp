import 'package:equatable/equatable.dart';

abstract class SourceEvent extends Equatable {
  const SourceEvent();

  @override
  List<Object> get props => [];
}

class FetchData extends SourceEvent {
  final String type;

  FetchData({this.type});
  @override
  List<Object> get props => [type];

  @override
  String toString() => 'Fetch $type news';
}
