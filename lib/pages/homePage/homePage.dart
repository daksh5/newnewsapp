import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:new_news_app/commonWidget/customWidget.dart';
import 'package:new_news_app/models/articles.dart';
import 'package:new_news_app/pages/newsDetail/bloc/detail_bloc.dart';
import 'package:new_news_app/pages/newsDetail/bloc/detail_event.dart';
import 'package:new_news_app/theme/theme.dart';
import 'bloc/bloc.dart';
import 'widget/newsCard.dart';

class HomePage extends StatelessWidget {
  Widget _headerNews(Articles article) {
    return Builder(
      builder: (context) {
        return InkWell(
            onTap: () {
              // ignore: close_sinks
              final detailBloc = BlocProvider.of<DetailBloc>(context);
              detailBloc.add(SelectNewsForDetail(article: article));
              Navigator.pushNamed(context, '/detail');
            },
            child: Stack(
              alignment: Alignment.bottomCenter,
              children: <Widget>[
                Hero(
                  tag: 'headerImage',
                  child: customImage(article.urlToImage),
                ),
                Container(
                  padding:
                      EdgeInsets.only(left: 20, right: 10, bottom: 10, top: 0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(article.title,
                          style: AppTheme.h1Style.copyWith(
                              color: Theme.of(context).colorScheme.surface)),
                      Text(
                         article.getDateOnly(),
                          style: AppTheme.subTitleStyle
                              .copyWith(color: Theme.of(context).disabledColor))
                    ],
                  ),
                )
              ],
            ));
      },
    );
  }

  HomePage();

  Widget _body(
    BuildContext context,
    List<Articles> list, {
    String type,
  }) {
    return CustomScrollView(
      slivers: <Widget>[
        SliverAppBar(
          centerTitle: true,
          title: Text(
            '${type.toUpperCase()} NEWS',
            style: AppTheme.h2Style
                .copyWith(color: Theme.of(context).textSelectionColor),
          ),
          backgroundColor: Theme.of(context).bottomAppBarColor,
          pinned: true,
        ),
        SliverToBoxAdapter(
          child: _headerNews(list.first),
        ),
        SliverList(
            delegate: SliverChildBuilderDelegate(
                (context, index) => NewsCard(
                      article: list[index],
                      type: type.toUpperCase(),
                    ),
                childCount: list.length))
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        systemNavigationBarColor: Theme.of(context).primaryColorDark,
        statusBarColor: Theme.of(context).primaryColorDark));
    return Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        body: SafeArea(
            child: BlocBuilder<NewsBloc, NewsState>(builder: (context, state) {
          if (state == null) {
            return Center(child: Text('Null block'));
          }
          if (state is Failure) {
            return Center(child: Text('Something went wrong'));
          }
          if (state is Loaded) {
            if (state.items == null || state.items.isEmpty) {
              return Text('No content available');
            } else {
              return _body(context, state.items, type: state.type);
            }
          } else {
            return Center(child: CircularProgressIndicator());
          }
        })));
  }
}
