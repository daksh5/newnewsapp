import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:new_news_app/commonWidget/customWidget.dart';
import 'package:new_news_app/models/articles.dart';
import 'package:new_news_app/pages/newsDetail/bloc/detail_bloc.dart';
import 'package:new_news_app/pages/newsDetail/bloc/detail_event.dart';
import 'package:new_news_app/pages/sourceNewsPage/bloc/newsource_bloc.dart';
import 'package:new_news_app/pages/sourceNewsPage/bloc/newsource_state.dart';
import 'package:new_news_app/pages/sources/bloc/sourceBloc.dart';
import 'package:new_news_app/pages/sources/bloc/sourceState.dart';
import 'package:new_news_app/theme/theme.dart';
import 'widget/newsCard.dart';

class SourceNewsPage extends StatelessWidget {
  Widget _body(BuildContext context, List<Articles> list, String type) {
    List<Articles> listNew =
        list.where((element) => element.source.name == type).toList();
    print(type);
    print(listNew);
    return CustomScrollView(
      slivers: <Widget>[
        SliverAppBar(
          centerTitle: true,
          title: Text(
            type.toUpperCase(),
            style: AppTheme.h2Style
                .copyWith(color: Theme.of(context).textSelectionColor),
          ),
          backgroundColor: Theme.of(context).bottomAppBarColor,
          pinned: true,
        ),
        SliverList(
            delegate: SliverChildBuilderDelegate(
                (context, index) => NewsCard(
                      article: listNew[index],
                      type: type.toUpperCase(),
                    ),
                childCount: listNew.length))
      ],
    );
  }

  @override
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        body: SafeArea(child: BlocBuilder<NewSourceBloc, DetailState>(
          builder: (context, state) {
            if (state == null) {
              return Center(child: Text('Null bloc'));
            }
            if (state is FailureDetail) {
              return Center(child: Text('Something went wrong'));
            }
            if (state is LoadedArticle) {
              if (state.selectedArticle == null) {
                return Text('No content available');
              } else {
                return _body(context, state.selectedArticle, state.type);
              }
            } else {
              return Center(child: CircularProgressIndicator());
            }
          },
        )));
  }
}
