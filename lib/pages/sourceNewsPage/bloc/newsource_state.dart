import 'package:equatable/equatable.dart';
import 'package:new_news_app/models/articles.dart';

abstract class DetailState extends Equatable {
  const DetailState();

  @override
  List<Object> get props => [];
}

class LoadingDetails extends DetailState {}

class FailureDetail extends DetailState {}

class LoadedArticle extends DetailState {
  final List<Articles> selectedArticle;
  final String type;


  LoadedArticle({this.selectedArticle,this.type});

  @override
  List<Object> get props => [selectedArticle];

}
