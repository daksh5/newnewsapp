import 'package:equatable/equatable.dart';
import 'package:new_news_app/models/articles.dart';

abstract class NewsDetailEvent extends Equatable {
  const NewsDetailEvent();

  @override
  List<Object> get props => [];
}

class SelectNewsForDetail extends NewsDetailEvent {
  final List<Articles> article;
  final String type;
  const SelectNewsForDetail({this.article,this.type});
}

class SelectNewsForDetailed extends NewsDetailEvent {
  final List<Articles> article;
  final String type;
  const SelectNewsForDetailed({this.article,this.type});
}
