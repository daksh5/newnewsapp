import 'package:flutter_bloc/flutter_bloc.dart';

import 'newsource_event.dart';
import 'newsource_state.dart';

class NewSourceBloc extends Bloc<NewsDetailEvent, DetailState> {
  NewSourceBloc(DetailState initialState) : super(initialState);

  @override
  Stream<DetailState> mapEventToState(NewsDetailEvent event) async* {
    if (event is SelectNewsForDetail) {
      try {
        yield LoadedArticle(selectedArticle: event.article,type: event.type);
      } catch (_) {
        yield FailureDetail();
      }
    }
  }
}
