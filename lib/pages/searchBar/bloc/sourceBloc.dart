import 'dart:async';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:new_news_app/resources/repository.dart';
import 'sourceEvent.dart';
import 'sourceState.dart';

class SearchBloc extends Bloc<SearchEvent, SearchState> {
  final Repository repository;

  SearchBloc(SearchState initialState, this.repository) : super(initialState);

  @override
  Stream<SearchState> mapEventToState(SearchEvent event) async* {
    if (event is FetchSearchData) {
      try {
        yield LoadingSearchData();
        final items = await repository.fetchAllNews(category: event.type);
        yield SearchDataLoaded(items: items, type: event.type);
      } catch (_) {
        yield FailedToLoadSearch();
      }
    }
  }
}
