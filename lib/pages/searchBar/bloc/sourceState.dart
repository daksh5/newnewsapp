import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:new_news_app/models/articles.dart';

abstract class SearchState extends Equatable {
  const SearchState();

  @override
  List<Object> get props => [];
}

class LoadingSearchData extends SearchState {}

class SearchDataLoaded extends SearchState {
  final List<Articles> items;
  final String type;

  const SearchDataLoaded({@required this.items, this.type});

  @override
  List<Object> get props => [items];

  @override
  String toString() => 'Loaded { items: ${items.length} }';
}

class FailedToLoadSearch extends SearchState {}
