import 'package:equatable/equatable.dart';

abstract class SearchEvent extends Equatable {
  const SearchEvent();

  @override
  List<Object> get props => [];
}

class FetchSearchData extends SearchEvent {
  final String type;

  FetchSearchData({this.type});
  @override
  List<Object> get props => [type];

  @override
  String toString() => 'Fetch $type news';
}
