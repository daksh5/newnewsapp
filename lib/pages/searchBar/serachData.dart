import 'package:flappy_search_bar/flappy_search_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:new_news_app/models/articles.dart';
import 'package:new_news_app/pages/homePage/widget/newsCard.dart';
import 'package:new_news_app/pages/searchBar/bloc/bloc.dart';
import 'package:new_news_app/pages/sourceNewsPage/bloc/newsource_bloc.dart';
import 'package:new_news_app/pages/sourceNewsPage/bloc/newsource_event.dart';
import 'package:new_news_app/pages/sources/bloc/bloc.dart';
import 'package:new_news_app/pages/sources/bloc/sourceState.dart';
import 'package:new_news_app/theme/theme.dart';

class SearchPage extends StatefulWidget {
  final PageController controller;

  SearchPage({Key key, this.controller}) : super(key: key);

  @override
  _SourcePageState createState() => _SourcePageState();
}

class _SourcePageState extends State<SearchPage> {

  Widget _body(
    BuildContext context,
    List<Articles> list, {
    String type,
  }) {
    var map1 = Map.fromIterable(list, key: (e) => e.source.name);
    print(map1);
    return CustomScrollView(
      slivers: <Widget>[
        SliverAppBar(
          centerTitle: true,
          title: Text(
            'News Sources',
            style: AppTheme.h2Style
                .copyWith(color: Theme.of(context).textSelectionColor),
          ),
          backgroundColor: Theme.of(context).bottomAppBarColor,
          pinned: true,
        ),
        SliverList(
            delegate: SliverChildBuilderDelegate(
                (context, index) =>
                    NewsCard(
                      article: list[index],
                      type: type.toUpperCase(),
                    ),
                childCount: list.length))
      ],
    );
  }

  Widget _emptybody() {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: SearchBar(
            iconActiveColor: Colors.white,
          icon: Icon(Icons.search,color: Colors.white),
          textStyle: AppTheme.h6Style.copyWith(color: Colors.white),
          cancellationWidget: Text("Cancel"),
          emptyWidget: Text("Search News")),
        ),
      ),
    );
    /*Text('Something went wrong',
          style: AppTheme.h2Style.copyWith(color: Colors.black))
    ]);*/
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        systemNavigationBarColor: Theme.of(context).primaryColorDark,
        statusBarColor: Theme.of(context).primaryColorDark));
    return Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        body: SafeArea(child:
            BlocBuilder<SearchBloc, SearchState>(builder: (context, state) {
          if (state == null) {
            return Center(child: Text('Null block'));
          }
          if (state is FailedToLoadSearch) {
            return _emptybody();
          }
          if (state is SearchDataLoaded) {
            if (state.items == null || state.items.isEmpty) {
              return _emptybody();
            } else {
              return _body(context, state.items, type: state.type);
            }
          } else {
            return Center(child: CircularProgressIndicator());
          }
        })));
  }
}
