import 'package:equatable/equatable.dart';
import 'package:new_news_app/models/articles.dart';

abstract class NewsDetailEvent extends Equatable {
  const NewsDetailEvent();

  @override
  List<Object> get props => [];
}

class SelectNewsForDetail extends NewsDetailEvent {
  final Articles article;
  const SelectNewsForDetail({this.article});

  @override
  String toString() => 'SelectNewsForDetail Page { Title:: ${article.title} }';
}

class SelectNewsForDetailed extends NewsDetailEvent {
  final Articles article;
  const SelectNewsForDetailed({this.article});

  @override
  String toString() => 'SelectNewsForDetailed news { Title: ${article.title} }';
}
