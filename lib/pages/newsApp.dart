import 'package:flutter/material.dart';
import 'package:new_news_app/commonWidget/bottomNavigationBar.dart';
import 'package:new_news_app/pages/searchBar/serachData.dart';
import 'package:new_news_app/pages/sourceNewsPage/sourceNewPage.dart';
import 'package:new_news_app/pages/sources/sourcePage.dart';
import 'category/categoryPage.dart';
import 'homePage/homePage.dart';
import 'profile/profilePage.dart';

class NewsApp extends StatefulWidget {
  NewsApp({Key key}) : super(key: key);

  @override
  _NewsAppState createState() => _NewsAppState();
}

class _NewsAppState extends State<NewsApp> {
  PageController _controller = PageController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavigationBarWidget(controller: _controller),
      body: PageView(
        controller: _controller,
        children: <Widget>[
          HomePage(),
          CategoryPage(
            controller: _controller,
          ),
          SourcePage(),
          SearchPage(),
          ProfilePage()
        ],
      ),
    );
  }
}
