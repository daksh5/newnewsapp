import 'package:dio/dio.dart';
import 'package:http/http.dart';
import 'package:new_news_app/helpers/constants.dart';
import 'package:new_news_app/models/article.dart';
import 'package:new_news_app/models/articles.dart';

class NewsApiProvider {
  Client client = Client();
  final _apiKey = Constant.newsApiKey;

  Future<List<Articles>> fetchNewsList({String category = ''}) async {
    var url =
        "${Constant.baseUrl}${Constant.topHeadLine}?country=in&apiKey=$_apiKey&category=$category";
    print(url);
    try {
      var response = await Dio().get(url);
      print(response.data.toString());
      return Article.fromJson(response.data).articles;
    } catch (e) {
      print(e);
      return e;
    }
  }

  Future<List<Articles>> searchNewsList({String category = ''}) async {
    var url =
        "${Constant.baseUrl}${Constant.everything}?&apiKey=$_apiKey";
    print(url);
    try {
      var response = await Dio().get(url);
      print(response.data.toString());
      return Article.fromJson(response.data).articles;
    } catch (e) {
      print(e);
      return e;
    }
  }

}
