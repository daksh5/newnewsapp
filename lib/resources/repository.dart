import 'package:new_news_app/models/articles.dart';

import 'newsApiProvider.dart';

class Repository {
  final newsApiProvider = NewsApiProvider();

  Future<List<Articles>> fetchAllNews({String category = ''}) =>
      newsApiProvider.fetchNewsList(category: category);

  Future<List<Articles>> searchNewsList({String category = ''}) =>
      newsApiProvider.searchNewsList(category: category);
}
