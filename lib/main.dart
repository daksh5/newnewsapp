import 'package:bloc/bloc.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:new_news_app/homePage.dart';
import 'bloc/simple_bloc_delegate.dart';
import 'auth/provider.dart';
import 'auth/auth.dart';

final ThemeData themeData = ThemeData(
    canvasColor: Colors.lightGreenAccent, accentColor: Colors.greenAccent);

Future<void> main() async {
  Bloc.observer = SimpleBlocDelegate();
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MaterialApp(
    home: MyApp(),
    debugShowCheckedModeBanner: false,
    theme: themeData,
  ));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Provider(
      auth: Auth(),
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData.dark(),
        home: MyHomePage(),
        debugShowCheckedModeBanner: false
      ),
    );
  }
}
