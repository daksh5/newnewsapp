import 'package:equatable/equatable.dart';

import 'articles.dart';

class Article extends Equatable {
  final String status;
  final int totalResults;
  final List<Articles> articles;

  const Article({
    this.status,
    this.totalResults,
    this.articles,
  });

  factory Article.fromJson(Map<String, dynamic> json) {
    return Article(
      status: json['status'] as String,
      totalResults: json['totalResults'] as int,
      articles: (json['articles'] as List<dynamic>)
          ?.map((e) =>
              e == null ? null : Articles.fromJson(e as Map<String, dynamic>))
          ?.toList(),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'status': status,
      'totalResults': totalResults,
      'articles': articles?.map((e) => e?.toJson())?.toList(),
    };
  }

  @override
  bool get stringify => true;

  @override
  List<Object> get props => [status, totalResults, articles];
}
