import 'package:equatable/equatable.dart';

class Source extends Equatable {
  final dynamic id;
  final String name;

  const Source({this.id, this.name});

  factory Source.fromJson(Map<String, dynamic> json) {
    return Source(
      id: json['id'],
      name: json['name'] as String,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'name': name,
    };
  }

  @override
  bool get stringify => true;

  @override
  List<Object> get props => [id, name];
}
